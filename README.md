## Connect Camera
This application is developed for users of the CyanogenMod 13.0 using a Samsung Galaxy S III (Intl) (Samsung - i9300) device.
It solves the "camera can't connect" problem. 

It's inspired by  https://play.google.com/store/apps/details?id=com.exlyo.camerarestarter

Because I haven't installed any google software at my mobile phone, I wasn't able to install this application.

Fortunately the author has published his code at https://github.com/androidseb/camerarestarter.

From this point I've developed my own personal minimalistic version of this software.

Because it has completely solved the camera problem, I want to provide this work to any interested CM13.0 i9300 user. 

The next important step is to include this app at f-droid...

## Warning
This application needs root access, because it kills the mediaserver process. 

!!! USE THIS APPLICATION AT YOUR OWN RISK !!!

Read first the (camera) relating threads at https://forum.cyanogenmod.org/forum/684-samsung-galaxy-s-iii-gsm-d2att-d2tmo-i9300/