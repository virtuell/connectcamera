package loc.virtuell.connectcamera;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ConnectCamera();
    }

    private void ConnectCamera()
    {
        try {
            ClearLogMessages();

            killMediaserver();

            AddLogMessage("Mediaserver terminated");

            startCamera();

            AddLogMessage("Camera started");

            closeApp();
        }
        catch (Throwable t) {
            AddLogMessage(t.getMessage());
        }
    }

    private void killMediaserver() throws Throwable
    {
        /*
            Sourcecode of this method by androidseb:
            https://github.com/androidseb/camerarestarter/blob/master/app/src/main/java/com/exlyo/camerarestarter/MainActivity.java
         */
        final Process p = Runtime.getRuntime().exec("su");
        DataOutputStream os = null;
        BufferedReader br = null;
        try {
            os = new DataOutputStream(p.getOutputStream());
            br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            os.writeBytes("line=$(ps|grep media.*[/]system[/]bin[/]mediaserver)" + "\n");
            os.writeBytes("echo ${line}" + "\n");
            String line = br.readLine();
            if (line.startsWith("media") && line.endsWith("/system/bin/mediaserver")) {
                line = line.replaceAll("media[ ]*", "");
                final int pid = Integer.parseInt(line.substring(0, line.indexOf(" ")));
                os.writeBytes("kill " + pid + "\n");
            }
        } finally {
            if (os != null) {
                try {
                    os.writeBytes("exit\n");
                } catch (Throwable t) {
                    t.printStackTrace();
                }
                try {
                    os.flush();
                } catch (Throwable t) {
                    t.printStackTrace();
                }
                try {
                    os.close();
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }

            if (br != null) {
                try {
                    br.close();
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }
    }

    private void startCamera() throws Throwable
    {
        /*
            Sourcecode of this method by androidseb:
            https://github.com/androidseb/camerarestarter/blob/master/app/src/main/java/com/exlyo/camerarestarter/MainActivity.java
         */
        final Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager pm = this.getPackageManager();
        final ResolveInfo mInfo = pm.resolveActivity(i, 0);
        final Intent cameraStartIntent = new Intent();

        cameraStartIntent.setComponent(new ComponentName(mInfo.activityInfo.packageName, mInfo.activityInfo.name));
        cameraStartIntent.setAction(Intent.ACTION_MAIN);
        cameraStartIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        cameraStartIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        this.startActivity(cameraStartIntent);

    }

    private void closeApp()
    {
        try {
            this.finish();
            System.exit(0);
        }
        finally {
        }

    }

    private void AddLogMessage(String message)
    {
        TextView logMessages = (TextView)findViewById(R.id.logMessages);
        logMessages.append(message + "\n");
    }

    private void ClearLogMessages()
    {
        TextView logMessages = (TextView)findViewById(R.id.logMessages);
        logMessages.setSingleLine(false);
        logMessages.setText("");
    }
}
